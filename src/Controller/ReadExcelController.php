<?php

namespace App\Controller;


use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ReadExcelController extends AbstractController
{
    /**
     * @Route("/read", name="read_excel")
     */
    public function readFile()
    {
        $inputFileName = '../public/uploads/file.xlsx';

        /** Load $inputFileName to a Spreadsheet Object  **/
        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
        $cellValue = $spreadsheet->getActiveSheet()
            ->rangeToArray(
                'B1:B1000',     // The worksheet range that we want to retrieve
                null,        // Value that should be returned for empty cells
                false,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                false,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                false         // Should the array be indexed by cell row and cell column
            );

        $listeType = $spreadsheet->getActiveSheet()
            ->rangeToArray(
                'I1:I1000',     // The worksheet range that we want to retrieve
                null,        // Value that should be returned for empty cells
                false,        // Should formulas be calculated (the equivalent of getCalculatedValue() for each cell)
                false,        // Should values be formatted (the equivalent of getFormattedValue() for each cell)
                false         // Should the array be indexed by cell row and cell column
            );

        $tabNom = array();
        $tabListe = array();

        foreach ($cellValue as $value){
            $tabNom[] = $value['0'];
        }
        foreach ($listeType as $value){
            $tabListe[] = $value['0'];
        }

        for ($i=0;$i < 1000;$i++){
            if (($tabNom[$i] == null) || ($tabNom[$i] == "TROPHEE SENIORS DU COUDRAY") || ($tabNom[$i] == "Liste des inscrits") || ($tabNom[$i] == "Nom et Prénom") || (strpos($tabNom[$i], "Page") === 0) || (strpos($tabNom[$i], "joueurs") === 4)){
                unset($tabNom[$i]);
            }
            if (($tabListe[$i] == null) || ($tabListe[$i] == "Rep.")){
                unset($tabListe[$i]);
            }
        }

        $tabFinal = array();

        $tabNom = array_values($tabNom);
        $tabListe = array_values($tabListe);

        for ($i=0;$i < sizeof($tabNom);$i++){
             $tabFinal[$i] = ["nom" => $tabNom[$i], "rep" => $tabListe[$i]];
        }


        $file = "../public/datas.json";
        //On ouvre le fichier datas.json
        $fileR= fopen($file, "r+");
        // On le vide
        ftruncate($fileR, 0);
        //On ferme le fichier
        fclose($fileR);
        //On lit le fichier datas.json et on encode en json notre tableau
        $current = file_get_contents($file);
        $current .= json_encode($tabFinal);
        //On écrit en Json dans le fichier
        file_put_contents($file, $current);
        return new Response(print ("Voici le tableau avec ". count($tabFinal) . " membres"),dd($tabFinal));
    }
}
