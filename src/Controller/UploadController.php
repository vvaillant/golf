<?php

namespace App\Controller;

use App\Entity\Form;
use App\Entity\Upload;
use App\Form\FormType;
use App\Form\UploadType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;


class UploadController extends AbstractController
{

    /**
     * @Route("/upload", name="upload_file")
     */
    public function uploadFile(Request $request){

        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);

        $form->handleRequest($request);
        // Tout formulaire est comme ça

        if ($form->isSubmitted() && $form->isValid()){
            $file = $upload->getNomFichier();
            $fileName = 'file.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $upload->setNomFichier($fileName);
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($upload);
            $entityManager->flush();

            return $this->redirectToRoute('read_excel');
        }

        return $this->render('upload/index.html.twig', array(
            'form' => $form->createView(),
        ));
    }

}
