<?php

namespace App\Repository;

use App\Entity\DecalageTrouParties;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method DecalageTrouParties|null find($id, $lockMode = null, $lockVersion = null)
 * @method DecalageTrouParties|null findOneBy(array $criteria, array $orderBy = null)
 * @method DecalageTrouParties[]    findAll()
 * @method DecalageTrouParties[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DecalageTrouPartiesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, DecalageTrouParties::class);
    }

    // /**
    //  * @return DecalageTrouParties[] Returns an array of DecalageTrouParties objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('d.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?DecalageTrouParties
    {
        return $this->createQueryBuilder('d')
            ->andWhere('d.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
