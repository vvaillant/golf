<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191017095052 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE competition (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, date VARCHAR(255) NOT NULL, starter VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE decalage_trou_parties (id INT AUTO_INCREMENT NOT NULL, parties_id INT DEFAULT NULL, trou_id INT DEFAULT NULL, decallage VARCHAR(255) NOT NULL, INDEX IDX_33A52F40362AAF23 (parties_id), INDEX IDX_33A52F401D222B9C (trou_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE golf (id INT AUTO_INCREMENT NOT NULL, lieu VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE par (id INT AUTO_INCREMENT NOT NULL, trou_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, temps INT NOT NULL, INDEX IDX_695DFBB41D222B9C (trou_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE parties (id INT AUTO_INCREMENT NOT NULL, competition_id INT DEFAULT NULL, nb_joueurs INT NOT NULL, INDEX IDX_436318057B39D312 (competition_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE trou (id INT AUTO_INCREMENT NOT NULL, golf_id INT DEFAULT NULL, temps_deplacement INT NOT NULL, INDEX IDX_5066A632F1503E2B (golf_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE upload (id INT AUTO_INCREMENT NOT NULL, nom VARCHAR(255) NOT NULL, lieu VARCHAR(255) NOT NULL, date DATE NOT NULL, nom_fichier VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE decalage_trou_parties ADD CONSTRAINT FK_33A52F40362AAF23 FOREIGN KEY (parties_id) REFERENCES parties (id)');
        $this->addSql('ALTER TABLE decalage_trou_parties ADD CONSTRAINT FK_33A52F401D222B9C FOREIGN KEY (trou_id) REFERENCES trou (id)');
        $this->addSql('ALTER TABLE par ADD CONSTRAINT FK_695DFBB41D222B9C FOREIGN KEY (trou_id) REFERENCES trou (id)');
        $this->addSql('ALTER TABLE parties ADD CONSTRAINT FK_436318057B39D312 FOREIGN KEY (competition_id) REFERENCES competition (id)');
        $this->addSql('ALTER TABLE trou ADD CONSTRAINT FK_5066A632F1503E2B FOREIGN KEY (golf_id) REFERENCES golf (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE parties DROP FOREIGN KEY FK_436318057B39D312');
        $this->addSql('ALTER TABLE trou DROP FOREIGN KEY FK_5066A632F1503E2B');
        $this->addSql('ALTER TABLE decalage_trou_parties DROP FOREIGN KEY FK_33A52F40362AAF23');
        $this->addSql('ALTER TABLE decalage_trou_parties DROP FOREIGN KEY FK_33A52F401D222B9C');
        $this->addSql('ALTER TABLE par DROP FOREIGN KEY FK_695DFBB41D222B9C');
        $this->addSql('DROP TABLE competition');
        $this->addSql('DROP TABLE decalage_trou_parties');
        $this->addSql('DROP TABLE golf');
        $this->addSql('DROP TABLE par');
        $this->addSql('DROP TABLE parties');
        $this->addSql('DROP TABLE trou');
        $this->addSql('DROP TABLE upload');
    }
}
