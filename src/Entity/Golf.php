<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\GolfRepository")
 */
class Golf
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lieu;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Trou", mappedBy="golf")
     */
    private $trous;

    public function __construct()
    {
        $this->trous = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLieu(): ?string
    {
        return $this->lieu;
    }

    public function setLieu(string $lieu): self
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * @return Collection|Trou[]
     */
    public function getTrous(): Collection
    {
        return $this->trous;
    }

    public function addTrous(Trou $trous): self
    {
        if (!$this->trous->contains($trous)) {
            $this->trous[] = $trous;
            $trous->setGolf($this);
        }

        return $this;
    }

    public function removeTrous(Trou $trous): self
    {
        if ($this->trous->contains($trous)) {
            $this->trous->removeElement($trous);
            // set the owning side to null (unless already changed)
            if ($trous->getGolf() === $this) {
                $trous->setGolf(null);
            }
        }

        return $this;
    }
}
