<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\DecalageTrouPartiesRepository")
 */
class DecalageTrouParties
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $decallage;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Parties", inversedBy="decalageTrouParties")
     */
    private $parties;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\trou", inversedBy="decalageTrouParties")
     */
    private $trou;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDecallage(): ?string
    {
        return $this->decallage;
    }

    public function setDecallage(string $decallage): self
    {
        $this->decallage = $decallage;

        return $this;
    }

    public function getParties(): ?Parties
    {
        return $this->parties;
    }

    public function setParties(?Parties $parties): self
    {
        $this->parties = $parties;

        return $this;
    }

    public function getTrou(): ?trou
    {
        return $this->trou;
    }

    public function setTrou(?trou $trou): self
    {
        $this->trou = $trou;

        return $this;
    }
}
