<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\PartiesRepository")
 */
class Parties
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbJoueurs;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Competition", inversedBy="parties")
     */
    private $competition;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\decalageTrouParties", mappedBy="parties")
     */
    private $decalageTrouParties;

    public function __construct()
    {
        $this->decalageTrouParties = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNbJoueurs(): ?int
    {
        return $this->nbJoueurs;
    }

    public function setNbJoueurs(int $nbJoueurs): self
    {
        $this->nbJoueurs = $nbJoueurs;

        return $this;
    }

    public function getCompetition(): ?Competition
    {
        return $this->competition;
    }

    public function setCompetition(?Competition $competition): self
    {
        $this->competition = $competition;

        return $this;
    }

    /**
     * @return Collection|decalageTrouParties[]
     */
    public function getDecalageTrouParties(): Collection
    {
        return $this->decalageTrouParties;
    }

    public function addDecalageTrouParty(decalageTrouParties $decalageTrouParty): self
    {
        if (!$this->decalageTrouParties->contains($decalageTrouParty)) {
            $this->decalageTrouParties[] = $decalageTrouParty;
            $decalageTrouParty->setParties($this);
        }

        return $this;
    }

    public function removeDecalageTrouParty(decalageTrouParties $decalageTrouParty): self
    {
        if ($this->decalageTrouParties->contains($decalageTrouParty)) {
            $this->decalageTrouParties->removeElement($decalageTrouParty);
            // set the owning side to null (unless already changed)
            if ($decalageTrouParty->getParties() === $this) {
                $decalageTrouParty->setParties(null);
            }
        }

        return $this;
    }
}
