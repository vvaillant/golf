<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\TrouRepository")
 */
class Trou
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $tempsDeplacement;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\DecalageTrouParties", mappedBy="trou")
     */
    private $decalageTrouParties;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\par", mappedBy="trou")
     */
    private $par;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\golf", inversedBy="trous")
     */
    private $golf;

    public function __construct()
    {
        $this->decalageTrouParties = new ArrayCollection();
        $this->par = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTempsDeplacement(): ?int
    {
        return $this->tempsDeplacement;
    }

    public function setTempsDeplacement(int $tempsDeplacement): self
    {
        $this->tempsDeplacement = $tempsDeplacement;

        return $this;
    }

    /**
     * @return Collection|DecalageTrouParties[]
     */
    public function getDecalageTrouParties(): Collection
    {
        return $this->decalageTrouParties;
    }

    public function addDecalageTrouParty(DecalageTrouParties $decalageTrouParty): self
    {
        if (!$this->decalageTrouParties->contains($decalageTrouParty)) {
            $this->decalageTrouParties[] = $decalageTrouParty;
            $decalageTrouParty->setTrou($this);
        }

        return $this;
    }

    public function removeDecalageTrouParty(DecalageTrouParties $decalageTrouParty): self
    {
        if ($this->decalageTrouParties->contains($decalageTrouParty)) {
            $this->decalageTrouParties->removeElement($decalageTrouParty);
            // set the owning side to null (unless already changed)
            if ($decalageTrouParty->getTrou() === $this) {
                $decalageTrouParty->setTrou(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|par[]
     */
    public function getPar(): Collection
    {
        return $this->par;
    }

    public function addPar(par $par): self
    {
        if (!$this->par->contains($par)) {
            $this->par[] = $par;
            $par->setTrou($this);
        }

        return $this;
    }

    public function removePar(par $par): self
    {
        if ($this->par->contains($par)) {
            $this->par->removeElement($par);
            // set the owning side to null (unless already changed)
            if ($par->getTrou() === $this) {
                $par->setTrou(null);
            }
        }

        return $this;
    }

    public function getGolf(): ?golf
    {
        return $this->golf;
    }

    public function setGolf(?golf $golf): self
    {
        $this->golf = $golf;

        return $this;
    }
}
