<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerBHGm4Nr\srcApp_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerBHGm4Nr/srcApp_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerBHGm4Nr.legacy');

    return;
}

if (!\class_exists(srcApp_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerBHGm4Nr\srcApp_KernelDevDebugContainer::class, srcApp_KernelDevDebugContainer::class, false);
}

return new \ContainerBHGm4Nr\srcApp_KernelDevDebugContainer([
    'container.build_hash' => 'BHGm4Nr',
    'container.build_id' => 'aa8470ab',
    'container.build_time' => 1571576064,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerBHGm4Nr');
