<?php

namespace App\Tests\Utils;

use App\Utils\UtilJson;
use PHPUnit\Framework\TestCase;

class UtilJsonTest extends TestCase
{
    public function testNbJoueursTotal()
    {
        $uh = new UtilJson();

        $json = file_get_contents("../../public/datas.json");

        $parsed_json = json_decode($json);
        $expectedJson = 118;
        $this->assertEquals($expectedJson, $uh->toJsonCount($parsed_json));
    }

    public function testEmptyFile()
    {
        $uh = new UtilJson();

        $json = file_get_contents("../../public/datas.json");

        $parsed_json = json_decode($json);
        $expectedJson = 0;
        $this->assertEquals($expectedJson, $uh->toJsonCount($parsed_json));
    }

    public function testRep()
    {
        $uh = new UtilJson();

        $json = file_get_contents("../../public/datas.json");
        $parsed_json = json_decode($json);
        $debut = 0;

        foreach ($parsed_json[$debut] as $key => $value){
            if ($key == 'rep'){
                $rep = $value;
            }
        }

        $expectedJson = 42;
        $this->assertEquals($expectedJson, $uh->CountRepJoueur($parsed_json, $rep, $debut));
    }
}
